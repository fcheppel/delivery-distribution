import axios from "axios";
import authService from "../features/Auth/AuthorizeService";


export function createAuthClient(baseURL) {
  const client = axios.create({
    baseURL: baseURL,
    headers: {
      "Content-type": "application/json"
    }
  })

  client.interceptors.request.use(async function (config) {
    const token = await authService.getAccessToken();
    if (!!token)
      config.headers.Authorization = `Bearer ${token}`;
    return config;
  })

  return client
}

export function createClient() {
    const client =  axios.create({
      baseURL: process.env.REACT_APP_API_URL,
      withCredentials: true,
      headers: {
        "Content-type": "application/json"
      }
    });

    client.interceptors.request.use(async function (config) {
      const token = await authService.getAccessToken();
      if (!!token)
        config.headers.Authorization = `Bearer ${token}`;
      return config;
  });

  return client;
}