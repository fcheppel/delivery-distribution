import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Spinner, Container } from "react-bootstrap";
import OrderCard from "./orderCard";


import {
    selectOrderIds,
    fetchOrdersByCustomer,
} from "./orderSlice"

const OrdersList = () =>{

    const dispatch = useDispatch();
    const orderedOrderIds = useSelector(selectOrderIds);

    const orderStatus = useSelector((state) => state.orders.status);
    const error = useSelector((state) => state.orders.error);

    useEffect(() => {        
        if (orderStatus === 'idle') {
          dispatch(fetchOrdersByCustomer())
        }
      }, [orderStatus, dispatch]);


    let content;

    if (orderStatus === 'loading') {
        content = <Spinner animation="border" variant="primary" />
      } else if (orderStatus === 'succeeded') {
        content = orderedOrderIds.map((orderId) => (
          <OrderCard key={orderId} orderId={orderId}/>
        ))
      } else if (orderStatus === 'error') {
        content = <div>{error}</div>
      }
      
    return(
        <Container className="vertical-center">
            <h1>Мои заказы</h1>
            <br/>
            {content}  
        </Container>
    );
}

export default OrdersList;