import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    useParams,
    Link
} from "react-router-dom";
import { Container, Badge, Row, Col, Card, Button } from "react-bootstrap";
import {
    getOrderById,
    setPaidById,
    selectedOrderUpdated
} from "./orderSlice";
import DeliveriesList from '../Deliveries/deliveriesList';
import OrderService from "./orderService";
import {deliveriesUpdated} from "../Deliveries/deliveriesSlice";


const OrderDetails = () =>{

    const {orderId} = useParams();
   
    const dispatch = useDispatch();
    
    const selectedOrder = useSelector((state) => state.orders.selectedOrder);
    const [orderRequestStatus, setOrderRequestStatus] = useState("idle");
    const [paidRequestStatus, setPaidRequestStatus] = useState("idle");
    const [cancelRequestStatus, setCancelRequestStatus] = useState("idle");

    const canSetPaid = 
        paidRequestStatus === "idle"
        && selectedOrder.isPaid === false;

    const canSetCanceled = 
        cancelRequestStatus === "idle"
        && selectedOrder.orderState.id !== 6;


    const setPaid = () =>{
        if(canSetPaid){
            dispatch(setPaidById(selectedOrder.id));
            setPaidRequestStatus("succeeded");
        }
        
    };

    const setCanceled = async () =>{
        if(canSetCanceled){
            try{
                await OrderService.cancelOrderById(selectedOrder.id);
                setCancelRequestStatus("succeeded");
                setOrderRequestStatus("idle");
                dispatch(deliveriesUpdated());
                dispatch(selectedOrderUpdated());
            }
            catch(err){
                console.error('Failed to cancel order: ', err);
            }
        }
    };

    useEffect(() =>{
        if(orderRequestStatus ==="idle"){
            dispatch(getOrderById(orderId));
            setOrderRequestStatus("succeeded");
        }
    }, [orderRequestStatus, orderId, dispatch]);

    return(
        <Container >
            <Link to="/orderList">
                К списку заказов
            </Link>
            <br/>
            <br/>
            <h5>{`Заказ № ${selectedOrder.code}`}</h5>
            <p>{`от ${new Date(selectedOrder.createdOn).toLocaleDateString()}`}</p>
            <Card bg="light" border="light">
                <Card.Body >
                    <Row>
                        <Col>
                            <p className="font-weight-bold">Адрес отправления</p>
                            <p>{selectedOrder.departureAddress}</p>
                        </Col>
                        <Col>
                            <p className="font-weight-bold">Текущий статус</p>
                            <Badge variant="primary">{selectedOrder.orderState.state}</Badge>
                        </Col>
                        <Col>
                            <p className="font-weight-bold">Вес заказа</p>
                            <p>{`${selectedOrder.weight} кг.`}</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <p className="font-weight-bold">Адрес получения</p>
                            <p>{selectedOrder.destinationAddress}</p>
                        </Col>
                        <Col>
                            <p className="font-weight-bold">Статус обновлён</p>
                            <p>{new Date(selectedOrder.updatedOn).toLocaleString()}</p>
                        </Col>
                        <Col>
                            <Row>
                                <Col className="d-flex justify-content-between">
                                    <p className="font-weight-bold">{selectedOrder.isPaid? "Оплачено" : "Ожидает оплаты"}</p>
                                    <p className="font-weight-bold">{`${selectedOrder.price} ₽`}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="d-flex justify-content-end">
                                    {canSetPaid ? 
                                        <Button 
                                            size="sm" 
                                            className="mx-1" 
                                            onClick={setPaid}
                                            disabled={!canSetPaid}>Оплатить</Button> 
                                        : <></>}
                                    {canSetCanceled ?
                                        <Button 
                                            variant="danger"
                                            size="sm" 
                                            className="mx-1"
                                            disabled={!canSetCanceled}
                                            onClick={setCanceled}>Отменить</Button>
                                        : <></>}
                                    
                                </Col>
                            </Row>
                        </Col>
                        
                    </Row>
                </Card.Body>   
            </Card>
            <br/>
            <DeliveriesList orderId={orderId}/>
        </Container>
    );
};

export default OrderDetails;