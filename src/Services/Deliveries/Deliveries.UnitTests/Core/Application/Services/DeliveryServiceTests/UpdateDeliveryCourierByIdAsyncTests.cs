﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Exceptions;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class UpdateDeliveryCourierByIdAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task UpdateDeliveryCourierByIdAsync_EntityNotFound_ThrowsNotFoundException(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var deliveryId = DeliveryBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(deliveryId))
                .ReturnsAsync(default(Delivery));
            
            //act
            Func<Task> act = async () =>
                await sut.UpdateDeliveryCourierByIdAsync(deliveryId, It.IsAny<Guid>());
            
            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task UpdateDeliveryCourierByIdAsync_DeliveryExists_CourierIdUpdated(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase();
            var courierId = Guid.Parse("9bd0cf3f-f9cf-494d-9a22-ef2574896312");
            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await sut.UpdateDeliveryCourierByIdAsync(delivery.Id, courierId);
            
            //assert
            delivery.CourierId.Should().Be(courierId);
        }
    }
}