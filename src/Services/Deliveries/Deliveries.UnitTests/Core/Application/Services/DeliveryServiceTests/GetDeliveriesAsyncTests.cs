using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class GetDeliveriesAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetDeliveriesAsync_DeliveriesNotFound_ThrowsEntityNotFoundException(
             [Frozen] Mock<IRepository<Delivery>> mockRepository, 
             [Frozen] DeliveryService deliveryService
            )
        {
            //arrange
            mockRepository.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(new List<Delivery>());
            
            //act
            var result = await deliveryService.GetDeliveriesAsync();
            
            //assert
            result.Should().BeEmpty();
        }

        [Theory, AutoMoqData]
        //[Fact]
        public async Task GetDeliveriesAsync_DeliveryExists_ReturnDeliveries(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService
            )
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase();
            var deliveryList = new List<Delivery>()
            {
                delivery
            };
            mockRepository.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(deliveryList);

            //act
            var result = await deliveryService.GetDeliveriesAsync();
            
            //arrange
            result.Should().BeEquivalentTo(deliveryList);
        }
    }
}