using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Events;
using Deliveries.Core.Application.Exceptions;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using MediatR;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class EditDeliveryAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_DeliveryNotFound_ThrowsEntityNotFoundException(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var deliveryId = DeliveryBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(deliveryId))
                .ReturnsAsync(default(Delivery));
            
            //act
            Func<Task> act = async () =>
                await deliveryService.EditDeliveryAsync(deliveryId, It.IsAny<Delivery>());
            
            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();

        }

        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_DeliveryExists_UpdatedOnIsNow(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTimeProvider,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase();
            var editedDelivery = DeliveryBuilder
                .CreateBase()
                .WithEmptyId()
                .WithState(2);
            
            mockDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 02, 01));
            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);

            //act
            await deliveryService.EditDeliveryAsync(delivery.Id, editedDelivery);
            
            //assert
            delivery.UpdatedOn.Should().Be(mockDateTimeProvider.Object.CurrentDateTime);
            delivery.DeliveryStateId.Should().Be(editedDelivery.DeliveryStateId);
        }

        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_DeliveryExists_UpdateInvoked(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase();
            var editedDelivery = DeliveryBuilder
                .CreateBase()
                .WithEmptyId()
                .WithState(3);
            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await deliveryService.EditDeliveryAsync(delivery.Id, editedDelivery);
            
            //assert
            mockRepository.Verify(repo => 
                repo.UpdateAsync(delivery), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_AllDeliveryHaveSameState_AllDeliveriesStatesForOrderUpdatedEventPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var firstDelivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var secondDelivery = DeliveryBuilder.CreateBase()
                .WithState(2);
            var editedDelivery = DeliveryBuilder.CreateBase()
                .WithEmptyId()
                .WithState(2);

            var deliveryList = new List<Delivery>()
            {
                firstDelivery,
                secondDelivery
            };

            mockRepository.Setup(repo => repo.GetByIdAsync(firstDelivery.Id))
                .ReturnsAsync(firstDelivery);
            mockRepository.Setup(repo => repo.GetOnConditionAsync(It.IsAny<Expression<Func<Delivery, bool>>>()))
                .ReturnsAsync(deliveryList);
            
            //act
            await sut.EditDeliveryAsync(firstDelivery.Id, editedDelivery);
            
            //assert
            mockMediator.Verify(mediator => mediator.Publish(It.IsAny<AllDeliveriesStatesForOrderUpdatedEvent>(), default), Times.Once());
        }

        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_DeliveriesHasDifferentStates_AllDeliveriesStatesForOrderUpdatedEventNotPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var firstDelivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var secondDelivery = DeliveryBuilder.CreateBase()
                .WithState(2);
            var editedDelivery = DeliveryBuilder.CreateBase()
                .WithEmptyId()
                .WithState(3);

            var deliveryList = new List<Delivery>()
            {
                firstDelivery,
                secondDelivery
            };

            mockRepository.Setup(repo => repo.GetByIdAsync(firstDelivery.Id))
                .ReturnsAsync(firstDelivery);
            mockRepository.Setup(repo => repo.GetOnConditionAsync(It.IsAny<Expression<Func<Delivery, bool>>>()))
                .ReturnsAsync(deliveryList);
            
            //act
            await sut.EditDeliveryAsync(firstDelivery.Id, editedDelivery);
            
            //assert
            mockMediator.Verify(mediator => mediator.Publish(It.IsAny<AllDeliveriesStatesForOrderUpdatedEvent>(), default), Times.Never);
        }

        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_DeliveryStateChanged_DeliveryStateUpdatedEventPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var editedDelivery = DeliveryBuilder.CreateBase()
                .WithEmptyId()
                .WithState(2);

            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await sut.EditDeliveryAsync(delivery.Id, editedDelivery);
            
            //assert
            mockMediator.Verify(mediator => 
                mediator.Publish(
                    It.IsAny<DeliveryStateUpdatedEvent>(), 
                    It.IsAny<CancellationToken>()),
                times: Times.Once);
        }
        
        [Theory, AutoMoqData]
        public async Task EditDeliveryAsync_DeliveryStateNotChanged_DeliveryStateUpdatedEventNotPublished(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] DeliveryService sut)
        {
            //arrange
            var delivery = DeliveryBuilder.CreateBase()
                .WithState(1);
            var editedDelivery = DeliveryBuilder.CreateBase()
                .WithEmptyId()
                .WithState(1);

            mockRepository.Setup(repo => repo.GetByIdAsync(delivery.Id))
                .ReturnsAsync(delivery);
            
            //act
            await sut.EditDeliveryAsync(delivery.Id, editedDelivery);
            
            //assert
            mockMediator.Verify(mediator => 
                    mediator.Publish(
                        It.IsAny<DeliveryStateUpdatedEvent>(), 
                        It.IsAny<CancellationToken>()),
                times: Times.Never);
        }
    }
}