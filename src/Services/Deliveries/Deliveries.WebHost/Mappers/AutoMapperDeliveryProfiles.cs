using AutoMapper;
using Deliveries.Core.Domain;
using Deliveries.WebHost.Dto.Delivery;

namespace Deliveries.WebHost.Mappers
{
    public class AutoMapperDeliveryProfiles : Profile
    {
        public AutoMapperDeliveryProfiles()
        {
            CreateMap<Delivery, DeliveryShortResponse>();
            CreateMap<DeliveryShortResponse, Delivery>();

            CreateMap<Delivery, CreateDeliveryRequest>();
            CreateMap<CreateDeliveryRequest, Delivery>();

            CreateMap<Delivery, UpdateDeliveryRequest>();
            CreateMap<UpdateDeliveryRequest, Delivery>()
                .ForMember(dst => dst.DeliveryStateId,
                    opt =>
                        opt.MapFrom(src => src.State));

            CreateMap<Delivery, GetDeliveryFullResponse>();
            CreateMap<GetDeliveryFullResponse, Delivery>();
            
            CreateMap<Delivery, GetDeliveriesOfOneCourierShortResponse>();
            CreateMap<Delivery, GetDeliveriesOfOneOrderShortResponse>();

            CreateMap<DeliveryStateHistory, DeliveryStateHistoryResponse>();
        }
    }
}
