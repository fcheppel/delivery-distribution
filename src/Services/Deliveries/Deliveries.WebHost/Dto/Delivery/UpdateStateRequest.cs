﻿namespace Deliveries.WebHost.Dto.Delivery
{
    public record UpdateStateRequest
    {
        public int State { get; set; }
    }
}