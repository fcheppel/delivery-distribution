﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public class GetDeliveriesOfOneCourierShortResponse
    {
        /// <summary>
        /// Id курьера, выполняющего доставку.
        /// </summary>
        public Guid CourierId { get; set; }
        
        /// <summary>
        /// GUID доставки.
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Текущее состояние доставки.
        /// </summary>
        public StateResponse DeliveryState { get; set; }
    }
}