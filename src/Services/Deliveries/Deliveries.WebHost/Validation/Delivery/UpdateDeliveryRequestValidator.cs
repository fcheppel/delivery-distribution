﻿using Deliveries.WebHost.Dto.Delivery;
using FluentValidation;

namespace Deliveries.WebHost.Validation.Delivery
{
    public class UpdateDeliveryRequestValidator:AbstractValidator<UpdateDeliveryRequest>
    {
        public UpdateDeliveryRequestValidator()
        {
            RuleFor(req => req.State).GreaterThanOrEqualTo(0);
            RuleFor(req => req.CourierId).NotEmpty();
        }
    }
}