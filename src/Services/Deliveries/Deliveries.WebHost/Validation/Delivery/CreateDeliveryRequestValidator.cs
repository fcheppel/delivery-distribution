﻿using System;
using Deliveries.WebHost.Dto.Delivery;
using FluentValidation;

namespace Deliveries.WebHost.Validation.Delivery
{
    public class CreateDeliveryRequestValidator:AbstractValidator<CreateDeliveryRequest>
    {
        public CreateDeliveryRequestValidator()
        {
            RuleFor(req => req.OrderId).NotEmpty();
            RuleFor(req => req.CourierId)
                .Must(CourierIdValidator)
                .WithMessage("'CourierId' must not be empty.");
        }

        private bool CourierIdValidator(Guid? courierId)
        {
            if (courierId == Guid.Empty)
            {
                return false;
            }
            
            return true;
        }
    }
}