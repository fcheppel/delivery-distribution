using System;
using System.Reflection;
using AutoMapper;
using Deliveries.Core.Abstraction;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Abstraction.Strategies;
using Deliveries.Core.Application;
using Deliveries.Core.Application.Integration.Consumers;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Application.Strategies;
using Deliveries.Core.Domain;
using Deliveries.DataAccess.Data;
using Deliveries.DataAccess.Data.DbInitializer;
using Deliveries.DataAccess.Repositories;
using Deliveries.WebHost.Helpers;
using Deliveries.WebHost.Options;
using FluentValidation.AspNetCore;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag.AspNetCore;
using Serilog;

namespace Deliveries.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration; 
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });
            
            services.AddControllers()
                .AddMvcOptions(options => options.SuppressAsyncSuffixInActionNames = false)
                .AddFluentValidation(opt =>
                {
                    opt.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    opt.LocalizationEnabled = false;
                });
            
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IRepository<Delivery>, DeliveriesRepository>();
            services.AddScoped<IRepository<DeliveryStateHistory>, DeliveryStateHistoryRepository>();
            services.AddScoped<ICurrentDateTimeProvider, CurrentDateTimeProvider>();
            
            services.AddScoped<IDeliveryService, DeliveryService>();
            services.AddScoped<IDeliveryStateHistoryService, DeliveryStateHistoryService>();
            services.AddScoped<ICreateDeliveriesStrategy, RandomCreateDeliveriesStrategy>();
            
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            
            services.AddDbContext<DataContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostgresConnection"));
            });


            services.AddCustomAuthentication(Configuration);
            services.AddCustomAuthorization();
            services.AddSwaggerWithAuth(Configuration);
            
            services.AddMassTransitRabbitMq(Configuration);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseExceptionHandler(err => err.UseCustomErrors(env));
            
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(options =>
            {
                options.DocExpansion = "list";
                options.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "deliveriesswaggerui",
                    AppName = "Deliveries Swagger UI"
                };
            });
            
            //app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseCors();
            
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers()
                    .RequireAuthorization(Policies.DeliveriesAccess);
            });
            
            dbInitializer.InitializeDb();
        }
    }
}