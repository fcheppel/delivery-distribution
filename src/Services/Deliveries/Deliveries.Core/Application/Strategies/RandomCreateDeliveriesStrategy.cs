﻿using System;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Abstraction.Strategies;
using Deliveries.Core.Domain;
using Deliveries.Core.Enumerations;

namespace Deliveries.Core.Application.Strategies
{
    public class RandomCreateDeliveriesStrategy: ICreateDeliveriesStrategy
    {

        private readonly IDeliveryService _deliveryService;

        public RandomCreateDeliveriesStrategy(IDeliveryService deliveryService)
        {
            _deliveryService = deliveryService;
        }

        public async Task CreateAsync(Guid orderId, double weight)
        {
            var deliveryCount = new Random().Next(1, (int) Math.Max(1, weight/10));
            for (int iDelivery = 0; iDelivery < deliveryCount; ++iDelivery)
            {
                await _deliveryService.AddDeliveryAsync(new Delivery()
                {
                    OrderId = orderId,
                    DeliveryStateId = DeliveryState.Processing.Id
                });
            }
        }
    }
}