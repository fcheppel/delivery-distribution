﻿using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Services;
using Deliveries.Core.Enumerations;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Deliveries.Core.Application.Integration.Consumers
{
    public class OrderStateUpdatedEventConsumer
        :IConsumer<OrderStateUpdated>
    {
        private ILogger<OrderStateUpdatedEventConsumer> _logger;
        private IDeliveryService _deliveryService;

        public OrderStateUpdatedEventConsumer(
            ILogger<OrderStateUpdatedEventConsumer> logger,
            IDeliveryService deliveryService)
        {
            _logger = logger;
            _deliveryService = deliveryService;
        }

        public async Task Consume(ConsumeContext<OrderStateUpdated> context)
        {
            var message = context.Message;
            _logger.LogInformation("{@OrderStateUpdated} message consumed from broker", message);
            
            //Если заказа отменён, то отменяем все доставки
            if (message.OrderStateId == 6)
            {
                var deliveries = await _deliveryService.GetOrderDeliveriesAsync(message.OrderId);
                foreach (var delivery in deliveries)
                {
                    await _deliveryService.UpdateDeliveryStateByIdAsync(delivery.Id, DeliveryState.Canceled.Id);
                }
            }
        }
    }
}