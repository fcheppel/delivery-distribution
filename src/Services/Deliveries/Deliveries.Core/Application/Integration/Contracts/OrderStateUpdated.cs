﻿using System;

// ReSharper disable once CheckNamespace
namespace DeliveryDistribution.Integration.Contracts
{
    // ReSharper disable once InconsistentNaming
    public interface OrderStateUpdated
    {
        public Guid OrderId { get; set; }
        
        public int OrderStateId { get; set; }
    }
}