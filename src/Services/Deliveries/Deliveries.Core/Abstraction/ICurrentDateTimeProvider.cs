using System;

namespace Deliveries.Core.Abstraction
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}