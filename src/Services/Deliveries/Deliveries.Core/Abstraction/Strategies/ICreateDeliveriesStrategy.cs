﻿using System;
using System.Threading.Tasks;

namespace Deliveries.Core.Abstraction.Strategies
{
    public interface ICreateDeliveriesStrategy
    {
        Task CreateAsync(Guid orderId, double weight);
    }
}