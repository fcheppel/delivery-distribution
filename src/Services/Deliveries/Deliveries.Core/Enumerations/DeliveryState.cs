namespace Deliveries.Core.Enumerations
{
    public class DeliveryState:Enumeration
    {
        public static DeliveryState Undefined = new DeliveryState(0, nameof(Undefined));
        public static DeliveryState Processing = new DeliveryState(1, "В работе");
        public static DeliveryState AssignToCourier = new DeliveryState(2, "Назначен курьеру");
        public static DeliveryState InProgress = new DeliveryState(3, "Доставляется");
        public static DeliveryState Delivered = new DeliveryState(4, "Доставлен");
        public static DeliveryState Canceled = new DeliveryState(5, "Отменена");
        
        public DeliveryState(int id, string name) : base(id, name)
        {
            
        }
    }
}