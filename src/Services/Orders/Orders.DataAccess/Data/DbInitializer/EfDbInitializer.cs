using System.Linq;
using Microsoft.EntityFrameworkCore;
using Orders.Core.Enumerations;

namespace Orders.DataAccess.Data.DbInitializer
{
    public class EfDbInitializer : IDbInitializer
    {

        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();

            if (!_dataContext.Orders.Any()
                || !_dataContext.OrderStates.Any()
                || !_dataContext.OrderStateHistories.Any())
            {
                _dataContext.Database.EnsureDeleted();
                _dataContext.Database.Migrate();
                
                _dataContext.OrderStates.AddRange(Enumeration.GetAll<OrderState>());

                _dataContext.Orders.AddRange(FakeDataFactory.Orders);
                
                _dataContext.OrderStateHistories.AddRange(FakeDataFactory.OrderStateHistories);

                _dataContext.SaveChanges();
                
            }
        }
    }
}