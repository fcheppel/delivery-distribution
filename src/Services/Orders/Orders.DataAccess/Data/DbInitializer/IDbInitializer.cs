namespace Orders.DataAccess.Data.DbInitializer
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}