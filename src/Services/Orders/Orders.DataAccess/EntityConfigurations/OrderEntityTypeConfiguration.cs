﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Orders.Core.Domain;

namespace Orders.DataAccess.EntityConfigurations
{
    public class OrderEntityTypeConfiguration
        :IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            //one-to-many Order-OrderStateHistory
            builder
                .HasMany<OrderStateHistory>()
                .WithOne(history => history.Order)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}