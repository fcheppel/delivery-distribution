﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Domain;
using Orders.DataAccess.Data;

namespace Orders.DataAccess.Repositories
{
    public class OrderStateHistoryRepository:IRepository<OrderStateHistory>
    {
        private readonly DataContext _dataContext;

        public OrderStateHistoryRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<OrderStateHistory>> GetAllAsync()
        {
            return 
                await _dataContext.OrderStateHistories
                    .AsNoTracking()
                    .ToListAsync();
        }

        public async Task<OrderStateHistory?> GetByIdAsync(Guid id)
        {
            return await _dataContext.OrderStateHistories.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid> AddAsync(OrderStateHistory entity)
        {
            _dataContext.OrderStateHistories.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(OrderStateHistory entity)
        {
            _dataContext.OrderStateHistories.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(OrderStateHistory entity)
        {
            _dataContext.OrderStateHistories.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<OrderStateHistory>> GetOnConditionAsync(Expression<Func<OrderStateHistory, bool>> predicate)
        {
            return await _dataContext.OrderStateHistories
                .Include(h => h.OrderState)
                .AsNoTrackingWithIdentityResolution()
                .Where(predicate)
                .ToListAsync();
        }
    }
}