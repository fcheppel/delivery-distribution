﻿using System;
using System.Threading.Tasks;

namespace Orders.Core.Abstraction.Strategies
{
    public interface IUpdateOrderStateStrategy
    {
        Task UpdateOrderStateAsync(Guid orderId, int deliveryStateId);
    }
}