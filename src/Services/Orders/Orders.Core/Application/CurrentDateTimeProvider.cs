using System;
using Orders.Core.Abstraction;

namespace Orders.Core.Application
{
    public class CurrentDateTimeProvider:ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}