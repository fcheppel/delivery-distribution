﻿using FluentValidation;
using Orders.WebHost.Dto.Order;

namespace Orders.WebHost.Validation.Order
{
    public class CreateOrEditOrderRequestValidator:AbstractValidator<CreateOrEditOrderRequest>
    {
        public CreateOrEditOrderRequestValidator()
        {
            RuleFor(req => req.Price).GreaterThanOrEqualTo(0);
            RuleFor(req => req.Weight).GreaterThanOrEqualTo(0);
            RuleFor(req => req.DepartureAddress).NotEmpty().Length(10, 200);
            RuleFor(req => req.DestinationAddress).NotEmpty().Length(10, 200);
        }
    }
}