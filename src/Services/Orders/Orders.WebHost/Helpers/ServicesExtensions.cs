﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using Orders.Core.Application.Integration.Consumers;
using Orders.WebHost.Options;

namespace Orders.WebHost.Helpers
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddMassTransitRabbitMq(
            this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = configuration.GetSection(RabbitMqOptions.SectionName).Get<RabbitMqOptions>();
            services.AddMassTransit(x =>
            {
                x.AddConsumer<AllDeliveriesStatesUpdatedIntegrationEventConsumer>();
                
                x.UsingRabbitMq(
                    (context, cfg) =>
                    {
                        cfg.Host(
                            rabbitSettings.Host,
                            rabbitSettings.Port,
                            rabbitSettings.VirtualHost,
                            conf =>
                            {
                                conf.Username(rabbitSettings.UserName);
                                conf.Password(rabbitSettings.Password);
                            });
                        cfg.ReceiveEndpoint(rabbitSettings.AllDeliveriesStatesUpdatedIntegrationEventQueue, e =>
                        {
                            e.ConfigureConsumer<AllDeliveriesStatesUpdatedIntegrationEventConsumer>(context);
                        });
                    }
                );
            });

            services.AddMassTransitHostedService();
            return services;
        }

        public static IServiceCollection AddCustomAuthentication(
            this IServiceCollection services, IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");
            
            var identityUrl = configuration.GetValue<string>("IdentityHostUrl");
            
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.Authority = identityUrl;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false
                    };
                });
            return services;
        }

        public static IServiceCollection AddCustomAuthorization(
            this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.OrdersAccess, policy =>
                    policy.RequireClaim("scope", "orders"));
            });
            return services;
        }

        public static IServiceCollection AddSwaggerWithAuth(
            this IServiceCollection services, IConfiguration configuration)
        {
            var identityExternalUrl = configuration.GetValue<string>("IdentityHostUrlExternal");
            services.AddOpenApiDocument(options =>
            {
                options.Title = "Orders API Doc";
                options.Version = "1.0";
                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit  = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = $"{identityExternalUrl}/connect/authorize",
                            TokenUrl = $"{identityExternalUrl}/connect/token",
                            Scopes = new Dictionary<string, string> { { "orders", "Orders API" } }
                        }
                    }
                });
                
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
                
            });
            return services;
        }
    }
}