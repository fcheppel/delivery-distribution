﻿namespace Orders.WebHost.Dto
{
    public record StateResponse
    {
        public int Id { get; set; }
        public string State { get; set; }
    }
}