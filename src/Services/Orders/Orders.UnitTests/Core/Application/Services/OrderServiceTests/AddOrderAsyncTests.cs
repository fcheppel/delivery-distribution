﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using MediatR;
using Microsoft.Extensions.Options;
using Moq;
using Orders.Core.Abstraction;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Application.Events;
using Orders.Core.Application.Services;
using Orders.Core.Domain;
using Orders.Core.Other;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Services.OrderServiceTests
{
    public class AddOrderAsyncTests
    {

        [Theory, AutoMoqData]
        public async Task AddOrderAsync_BaseRequest_AddIsInvoked(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] Mock<IOptions<OperationSettings>> mockOperationSettings,
            [Frozen] OrderService orderService            
            )
        {
            //arrange
            var order = OrderBuilder
                .CreateBase();

            var operationSettings = new OperationSettings()
            {
                OrderSettings = OrderBuilder.CreateSettings()
            };
            
            mockOperationSettings.Setup(settings => settings.Value)
               .Returns(operationSettings);
            
            //act
            await orderService.AddOrderAsync(order);

            //assert
            mockRepository.Verify(repo => repo.AddAsync(order), Times.Once);
        }
        [Theory, AutoMoqData]
        public async Task AddOrderAsync_CreatedOnIsNull_CreatedOnIsNow(
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTimeProvider,
            [Frozen] Mock<IOptions<OperationSettings>> mockOperationSettings,
            [Frozen] OrderService orderService
        )
        {
            //arrange
            var order = OrderBuilder
                .CreateBase();

            mockDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 02, 15));
            mockOperationSettings.Setup(settings => settings.Value)
                .Returns(new OperationSettings() { OrderSettings = OrderBuilder.CreateSettings() });
            //act
            await orderService.AddOrderAsync(order);

            //assert            
            order.CreatedOn.Should().Be(mockDateTimeProvider.Object.CurrentDateTime);
        }

        [Theory, AutoMoqData]
        public async Task AddOrderAsync_CorrectInput_OrderCreatedEventPublished(
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] Mock<IOptions<OperationSettings>> mockOperationSettings,
            [Frozen] OrderService sut)
        {
            //arrange
            var newOrder = OrderBuilder.CreateBase();
            mockOperationSettings.Setup(settings => settings.Value)
                .Returns(new OperationSettings() { OrderSettings = OrderBuilder.CreateSettings() });
            
            //act
            await sut.AddOrderAsync(newOrder);
            
            //assert
            mockMediator.Verify(mediator => mediator.Publish(
                It.IsAny<OrderCreatedEvent>(),
                It.IsAny<CancellationToken>()));
        }
        
        [Theory, AutoMoqData]
        public async Task AddOrderAsync_CorrectInput_OrderStateUpdatedEventPublished(
            [Frozen] Mock<IMediator> mockMediator,
            [Frozen] Mock<IOptions<OperationSettings>> mockOperationSettings,
            [Frozen] OrderService sut)
        {
            //arrange
            var newOrder = OrderBuilder.CreateBase();
            mockOperationSettings.Setup(settings => settings.Value)
                .Returns(new OperationSettings() { OrderSettings = OrderBuilder.CreateSettings() });
            
            //act
            await sut.AddOrderAsync(newOrder);
            
            //assert
            mockMediator.Verify(mediator => mediator.Publish(
                It.IsAny<OrderStateUpdatedEvent>(),
                It.IsAny<CancellationToken>()));
        }
    }
}
