using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Customers.Core.Domain;

namespace Customers.Core.Abstraction.Repositories
{
    public interface IRepository<T> where T:
        BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T?> GetByIdAsync(Guid id);

        Task<Guid> AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);

        Task<IEnumerable<T>> GetOnConditionAsync(Expression<Func<T, bool>> predicate);
    }
}