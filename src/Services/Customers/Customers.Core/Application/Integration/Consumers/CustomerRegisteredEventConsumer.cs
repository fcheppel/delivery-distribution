﻿using System;
using System.Threading.Tasks;
using Customers.Core.Abstraction.Services;
using Customers.Core.Domain;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Customers.Core.Application.Integration.Consumers
{
    public class CustomerRegisteredEventConsumer: IConsumer<CustomerRegistered>
    {
        private readonly ILogger<CustomerRegisteredEventConsumer> _logger;
        private readonly ICustomerService _customerService;

        public CustomerRegisteredEventConsumer(
            ILogger<CustomerRegisteredEventConsumer> logger,
            ICustomerService customerService)
        {
            _logger = logger;
            _customerService = customerService;
        }

        public async Task Consume(ConsumeContext<CustomerRegistered> context)
        {
            var message = context.Message;
            _logger.LogInformation("{@CustomerRegistered} message consumed from broker", message);
            var customer = new Customer()
            {
                Id = Guid.Parse(message.Id),
                Email = message.Email,
                Name = message.Name,
                PhoneNumber = message.PhoneNumber
            };
            await _customerService.AddCustomerAsync(customer);
        }
    }
}