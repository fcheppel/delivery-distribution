using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Customers.DataAccess.Data.DbInitializer
{
    public class EfDbInitializer : IDbInitializer
    {

        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();

            if (!_dataContext.Customers.Any())
            {
                _dataContext.Database.EnsureDeleted();
                _dataContext.Database.Migrate();
                
                _dataContext.Customers.AddRange(FakeDataFactory.Customers);
                
                _dataContext.SaveChanges();
            }
        }
    }
}