using System;
using System.Reflection;
using AutoMapper;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Abstraction.Services;
using Customers.Core.Application.Integration.Consumers;
using Customers.Core.Application.Services;
using Customers.Core.Domain;
using Customers.DataAccess.Data;
using Customers.DataAccess.Data.DbInitializer;
using Customers.DataAccess.Repositories;
using Customers.WebHost.Helpers;
using Customers.WebHost.Options;
using FluentValidation.AspNetCore;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag.AspNetCore;
using Serilog;

namespace Customers.WebHost
{
    public class Startup
    {
        
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration; 
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });
            
            services.AddControllers()
                .AddMvcOptions(options => options.SuppressAsyncSuffixInActionNames = false)
                .AddFluentValidation(opt =>
                {
                    opt.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    opt.LocalizationEnabled = false;
                });
            
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IRepository<Customer>, CustomersRepository>();
            services.AddScoped<ICustomerService, CustomerService>();
            
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            
            services.AddDbContext<DataContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostgresConnection"));
            });
            
            services.AddCustomAuthentication(Configuration);
            services.AddCustomAuthorization();

            services.AddSwaggerWithAuth(Configuration);

            services.AddMassTransitRabbitMq(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseExceptionHandler(err => err.UseCustomErrors(env));
            
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(options =>
            {
                options.DocExpansion = "list";
                options.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "customersswaggerui",
                    AppName = "Customers Swagger UI"
                };
            });
            
          //  app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseCors();
            
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers()
                    .RequireAuthorization(Policies.CustomersAccess);
            });
            
            dbInitializer.InitializeDb();
        }
    }

    public static class ServicesExtensions
    {
        public static IServiceCollection AddMassTransitRabbitMq(
            this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = configuration.GetSection(RabbitMqOptions.SectionName).Get<RabbitMqOptions>();
            services.AddMassTransit(x =>
            {
                x.AddConsumer<CustomerRegisteredEventConsumer>();

                x.UsingRabbitMq(
                    (context, cfg) =>
                    {
                        cfg.Host(
                            rabbitSettings.Host,
                            rabbitSettings.Port,
                            rabbitSettings.VirtualHost,
                            conf =>
                            {
                                conf.Username(rabbitSettings.UserName);
                                conf.Password(rabbitSettings.Password);
                            });
                        cfg.ReceiveEndpoint(rabbitSettings.CustomerRegisteredEventQueue,
                            e => { e.ConfigureConsumer<CustomerRegisteredEventConsumer>(context); });
                    }
                );
            });

            services.AddMassTransitHostedService();
            return services;
        }
    }
}