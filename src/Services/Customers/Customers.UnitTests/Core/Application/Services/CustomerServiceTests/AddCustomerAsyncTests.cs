﻿using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Application.Services;
using Customers.Core.Domain;
using Customers.UnitTests.Attributes;
using Customers.UnitTests.Builders.Domain;
using Moq;
using Xunit;

namespace Customers.UnitTests.Core.Application.Services.CustomerServiceTests
{
    public class AddCustomerAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task AddCustomerAsync_SuccessfullyExecuted_ShouldInvokeAddMethod(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
        )
        {
            //Arrange
            var customer = CustomerBuilder.CreateBase();
            mockRepository.Setup(repo => repo.AddAsync(customer)).ReturnsAsync(customer.Id);
                
            //Act
            await customerService.AddCustomerAsync(customer);

            //Assert
            mockRepository.Verify(repo => repo.AddAsync(customer), Times.Once);
        }
    }
}