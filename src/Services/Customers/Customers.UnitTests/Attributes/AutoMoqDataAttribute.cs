using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;

namespace Customers.UnitTests.Attributes
{
    /// <summary>
    /// Аттрибут для автоматического мока зависимотей и подстановки автомаппера
    /// </summary>
    public class AutoMoqDataAttribute: AutoDataAttribute
    {
        public AutoMoqDataAttribute()
            : base(() =>
            {
                var fixture = new Fixture().Customize(new AutoMoqCustomization());
                return fixture;
            })
        {
        }
   
    }
}