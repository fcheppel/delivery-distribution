﻿using System;
using System.Collections.Generic;
using System.Linq;
using Duende.IdentityServer.Extensions;
using Duende.IdentityServer.Models;
using IdentityServer.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IdentityServer.Controllers
{
    public class OidcConfigurationController : Controller
    {
        private readonly ILogger<OidcConfigurationController> _logger;
        private readonly IOptions<ExternalUrl> _externalUrlOptions;

        public OidcConfigurationController(
            ILogger<OidcConfigurationController> logger,
            IOptions<ExternalUrl> externalUrlOptions)
        {
            _logger = logger;
            _externalUrlOptions = externalUrlOptions;
        }
        
        [HttpGet("_configuration/{clientId}")]
        public IActionResult GetClientRequestParameters([FromRoute]string clientId)
        {
            var client = Config.GetClients(_externalUrlOptions.Value).FirstOrDefault(item => item.ClientId == clientId);
            if (client == null)
            {
                return NotFound();
            }
            
            var responseType = String.Empty;
            if (client.AllowedGrantTypes.Contains(GrantType.Implicit))
            {
                responseType += "id_token";
            }

            if (client.AllowAccessTokensViaBrowser)
            {
                responseType += " token";
            }
            
            return Ok(new Dictionary<string, string>
            {
                ["authority"] = _externalUrlOptions.Value.Authority,
                ["client_id"] = client.ClientId,
                ["redirect_uri"] = client.RedirectUris.First(),
                ["post_logout_redirect_uri"] = client.PostLogoutRedirectUris.First(),
                ["response_type"] = responseType,
                ["scope"] = string.Join(" ", client.AllowedScopes)
            });
        }
    }
}
