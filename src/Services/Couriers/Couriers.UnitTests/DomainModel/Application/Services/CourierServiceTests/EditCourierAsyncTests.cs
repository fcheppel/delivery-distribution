﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.UnitTests.Attributes;
using Couriers.UnitTests.Builders.DomainModel.Domain;
using Moq;
using Xunit;
using Couriers.Core.Application.Exceptions;

namespace Couriers.UnitTests.DomainModel.Application.Services.CourierServiceTests
{
    public class EditCourierAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task EditCourierAsync_CourierNotFound_ShouldThrowEntityNotFoundException(
            [Frozen] Mock<IRepository<Courier>> mockRepository,            
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id)).ReturnsAsync(default(Courier));
                
            //Act
            Func<Task> act = async () => await courierService.EditCourierAsync(courier.Id, It.IsAny<Courier>());

            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }
        
        [Theory, AutoMoqDataWithMapper]
        public async Task EditCourierAsync_SuccessfullyExecuted_ShouldInvokeEditMethod(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] Mock<IRepository<Vehicle>> mockVehicleRepository,
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            var vehicle = VehicleBuilder.CreateBase();

            var updatedCourier = CourierBuilder
                    .CreateBase()
                    .WithEmptyId()
                    .WithName("newName")
                    .WithPhone("11111");
            
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id)).ReturnsAsync(courier);
            mockVehicleRepository.Setup(repo => repo.GetByIdAsync(courier.VehicleId)).ReturnsAsync(vehicle);

            //Act
            await courierService.EditCourierAsync(courier.Id, updatedCourier);

            //Assert
            mockRepository.Verify(repo => repo.UpdateAsync(courier), Times.Once);
        }
    }
}