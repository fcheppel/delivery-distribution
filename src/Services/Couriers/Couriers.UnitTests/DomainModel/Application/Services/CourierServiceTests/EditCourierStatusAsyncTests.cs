﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.UnitTests.Attributes;
using Couriers.UnitTests.Builders.DomainModel.Domain;
using Moq;
using Xunit;
using Couriers.Core.Application.Exceptions;
using Couriers.Core.Domain.Enumerations;

namespace Couriers.UnitTests.DomainModel.Application.Services.CourierServiceTests
{
    public class EditCourierStatusAsyncTests
    {
        [Theory, AutoMoqDataWithMapper]
        public async Task EditCourierStatusAsync_CourierNotFound_ThrowsEntityNotFoundException(
           [Frozen] Mock<IRepository<Courier>> mockRepository,
           [Frozen] CourierService courierService)
        {
            //arrange
            var courierId = CourierBuilder.CreateBase().Id;
            var newStatusId = 2;
            mockRepository.Setup(repo => repo.GetByIdAsync(courierId))
                .ReturnsAsync(default(Courier));

            //act
            Func<Task> act = async () =>
                await courierService.EditCourierStatusAsync(courierId, newStatusId);

            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();

        }
        
        [Theory, AutoMoqData]
        public async Task EditCourierStatusAsync_CourierExists_StatusIsActive(
            [Frozen] Mock<IRepository<Courier>> mockRepository,            
            [Frozen] CourierService courierService)
        {
            //arrange
            var courier = CourierBuilder.CreateBase();
            var newStateId = CourierState.Active.Id;
                        
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id))
                .ReturnsAsync(courier);
            //act
            await courierService.EditCourierStatusAsync(courier.Id, newStateId);

            //assert
            courier.CourierStateId.Should().Be(newStateId);
        }

        [Theory, AutoMoqData]
        public async Task EditCourierStatusAsync_CourierExists_UpdateInvoked(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService)
        {
            //arrange
            var courier = CourierBuilder.CreateBase();
            var newStateId = CourierState.Active.Id;
            
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id))
                .ReturnsAsync(courier);

            //act
            await courierService.EditCourierStatusAsync(courier.Id, newStateId);

            //assert
            mockRepository.Verify(repo =>
                repo.UpdateAsync(It.IsAny<Courier>()), Times.Once);
        }
    }
}