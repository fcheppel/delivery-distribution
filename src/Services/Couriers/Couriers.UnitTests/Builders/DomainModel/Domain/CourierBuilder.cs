﻿using System;
using System.Collections.Generic;
using Couriers.Core.Domain;

namespace Couriers.UnitTests.Builders.DomainModel.Domain
{
    public static class CourierBuilder
    {
        public static Courier CreateBase()
        {
            return new Courier()
            {
                Id = Guid.Parse("fccbce2a-c1f9-45e6-b845-80360080db1c"),                
                Name = "Name",
                PhoneNumber = "12345",
                CourierStateId = 1,
                //VehicleId = Guid.Parse("312348d4-392a-4ed7-905d-13243f294900"),
                //Vehicle = new Vehicle()
                //            {
                //                Id = Guid.Parse("312348d4-392a-4ed7-905d-13243f294900"),
                //                Name = "Велосипед",
                //                LoadCapacity = 40
                //            }
            };
        }

        public static Courier WithEmptyId(this Courier courier)
        {
            courier.Id = Guid.Empty;
            return courier;
        }

        public static Courier WithName(this Courier courier, string name)
        {
            courier.Name = name;
            return courier;
        }

        public static Courier WithPhone(this Courier courier, string phone)
        {
            courier.PhoneNumber = phone;
            return courier;
        }        
    }
}