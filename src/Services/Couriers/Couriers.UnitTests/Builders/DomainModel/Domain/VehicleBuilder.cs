﻿using Couriers.Core.Domain;
using System;


namespace Couriers.UnitTests.Builders.DomainModel.Domain
{
    public class VehicleBuilder
    {
        public static Vehicle CreateBase()
        {
            return new Vehicle()
            {
                Id = Guid.Parse("312348d4-392a-4ed7-905d-13243f294900"),
                Name = "Велосипед",
                LoadCapacity = 40
                
            };
        }
    }
}
