﻿using System;
using System.Collections.Generic;
using Couriers.Core.Domain.Enumerations;


namespace Couriers.Core.Domain
{
    public class Courier : BaseEntity
    {
        /// <summary>
        /// Фамилия, имя и отчество.  
        /// </summary>
        public string? Name { get; set; }
        
        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string? PhoneNumber { get; set; }
        
        /// <summary>
        /// Id текущего статуса курьера
        /// </summary>
        public int CourierStateId { get; set; }
        
        /// <summary>
        /// Текущий статус курьера
        /// </summary>
        public virtual CourierState? CourierState { get; set; }
        /// <summary>
        /// Id транспортного средства курьера
        /// </summary>
        public Guid VehicleId { get; set; }

        /// <summary>
        /// Транспортнгое средство курьера
        /// </summary>
        public virtual Vehicle? Vehicle { get; set; }

    }
}
