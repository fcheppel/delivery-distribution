﻿using System;

namespace Couriers.Core.Application.Exceptions
{
    public class EntityEditException : Exception
    {
        public EntityEditException()
        {
        }
        
        public EntityEditException(Guid entityId) : base($"Failed to update entity with Id {entityId}")
        {
        }

        public EntityEditException(string? message) : base(message)
        {
        }

        public EntityEditException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}