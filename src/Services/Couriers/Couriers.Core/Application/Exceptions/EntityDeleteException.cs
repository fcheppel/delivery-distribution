﻿using System;

namespace Couriers.Core.Application.Exceptions
{
    public class EntityDeleteException : Exception
    {
        public EntityDeleteException()
        {
        }
        
        public EntityDeleteException(Guid entityId) : base($"Failed to delete entity with Id {entityId}")
        {
        }

        public EntityDeleteException(string? message) : base(message)
        {
        }

        public EntityDeleteException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}