using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace Couriers.DataAccess.Repositories
{
    public class CouriersRepository:IRepository<Courier>
    {
        private readonly DataContext _dataContext;

        public CouriersRepository(DataContext context)
        {
            _dataContext = context;
        }
        
        public async Task<IEnumerable<Courier>> GetAllAsync()
        {
            var entities = await _dataContext
                .Couriers
                .Include(d => d.CourierState)
                .Include(e => e.Vehicle)
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();
            return entities;
        }

        public async Task<Courier?> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext
                .Couriers
                .Include(d => d.CourierState)
                .Include(e => e.Vehicle)
                .FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public async Task<Guid> AddAsync(Courier entity)
        {
            _dataContext.Couriers.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(Courier entity)
        {
            _dataContext.Couriers.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Courier entity)
        {
            _dataContext.Couriers.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }
        
        public async Task<IEnumerable<Courier>> GetOnConditionAsync(Expression<Func<Courier, bool>> predicate)
        {
            return await _dataContext.Couriers
                .Include(d => d.CourierState)
                .Include(e => e.Vehicle)
                .AsNoTrackingWithIdentityResolution()
                .Where(predicate)
                .ToListAsync();
        }
    }
}