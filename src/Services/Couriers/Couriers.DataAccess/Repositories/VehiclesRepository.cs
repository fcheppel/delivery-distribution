﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace Couriers.DataAccess.Repositories
{
    public class VehiclesRepository : IRepository<Vehicle>
    {
        private readonly DataContext _dataContext;

        public VehiclesRepository(DataContext context)
        {
            _dataContext = context;
        }

        public async Task<IEnumerable<Vehicle>> GetAllAsync()
        {
            var entities = await _dataContext
                .Vehicles                
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();
            return entities;
        }

        public async Task<Vehicle?> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext
                .Vehicles
                .FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public async Task<Guid> AddAsync(Vehicle entity)
        {
            _dataContext.Vehicles.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(Vehicle entity)
        {
            _dataContext.Vehicles.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Vehicle entity)
        {
            _dataContext.Vehicles.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Vehicle>> GetOnConditionAsync(Expression<Func<Vehicle, bool>> predicate)
        {
            return await _dataContext.Vehicles                
                .AsNoTrackingWithIdentityResolution()
                .Where(predicate)
                .ToListAsync();
        }
    }
}