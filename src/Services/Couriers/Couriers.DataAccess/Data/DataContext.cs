using Couriers.Core.Domain;
using Couriers.Core.Domain.Enumerations;
using Couriers.DataAccess.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Couriers.DataAccess.Data
{
    public class DataContext: DbContext
    {
        
        
        public DbSet<Courier> Couriers { get; set; }
        
        
        public DbSet<CourierState> CourierStates { get; set; }
        
        public DbSet<Vehicle> Vehicles { get; set; }

        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //one-to-many Vehicle-Courier
            modelBuilder.Entity<Vehicle>()
                .HasMany(c => c.Couriers)
                .WithOne(d => d.Vehicle);

            modelBuilder.ApplyConfiguration(new CourierStateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);

        }
    }
}