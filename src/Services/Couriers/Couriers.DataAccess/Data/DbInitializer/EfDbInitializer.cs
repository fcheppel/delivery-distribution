using Microsoft.EntityFrameworkCore;
using System.Linq;
using Couriers.Core.Domain.Enumerations;
using Couriers.DataAccess.Data;

namespace Couriers.DataAccess.Data.DbInitializer
{
    public class EfDbInitializer : IDbInitializer
    {

        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();

            if (!_dataContext.Couriers.Any()                
                )
            {
                _dataContext.Database.EnsureDeleted();
                _dataContext.Database.Migrate();
                
                
                _dataContext.CourierStates.AddRange(Enumeration.GetAll<CourierState>());
                _dataContext.Vehicles.AddRange(FakeDataFactory.Vehicles);
                _dataContext.Couriers.AddRange(FakeDataFactory.Couriers);
                
                

                _dataContext.SaveChanges();
                
             
            }
        }
    }
}