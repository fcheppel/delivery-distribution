﻿using Couriers.Core.Domain;
using Couriers.Core.Domain.Enumerations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Couriers.DataAccess.EntityConfigurations
{
    public class VehicleEntityTypeConfiguration :
        IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)          
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Name)
                .HasMaxLength(200)
                .IsRequired();
                        
        }
    }
}
