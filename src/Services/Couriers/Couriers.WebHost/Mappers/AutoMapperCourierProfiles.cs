using AutoMapper;
using Couriers.Core.Domain;
using Couriers.WebHost.Dto.Courier;

namespace Couriers.WebHost.Mappers
{
    public class AutoMapperCourierProfiles : Profile
    {
        public AutoMapperCourierProfiles()
        {
            CreateMap<Courier, CourierShortResponse>()
                .ForMember(dst => dst.State,
                    opt => opt.MapFrom(src =>src.CourierState.Name));
            CreateMap<CourierShortResponse, Courier>();

            CreateMap<Courier, CreateOrEditCourierRequest>();            
            CreateMap<CreateOrEditCourierRequest, Courier>();
               

            CreateMap<Courier, CourierDetailedResponse>()
                .ForMember(dst => dst.State,
                    opt => opt.MapFrom(src => src.CourierState.Name));
            CreateMap<CourierDetailedResponse, Courier>();

        }
    }
}
